package pl.codementors.pracadomowa;

import java.util.Scanner;

/**
 * Homework task 1.
 *
 * @author Damir
 */
public class PierwszyMain {
    /**
     * Application main method.
     *
     * @param args Application starting parameters
     */

    public static void main(String[] args) {

        int number; //Command from user.
        Scanner scanner = new Scanner(System.in); //For getting user input.
        System.out.println("Please write a number,\n" +
                "number 42 finishing process:");

        //Application main loop.
        do {
            number = scanner.nextInt();
            if (number != 42) { //Adding and printing new note if condition is true.
                System.out.println("You wrote number: " + number);

            }
        }
        while (number != 42); //Exit application if condition is false.
        System.out.println("You wrote number 42, process finished.");

    }
}
