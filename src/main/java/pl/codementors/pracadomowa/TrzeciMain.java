package pl.codementors.pracadomowa;

import com.sun.xml.internal.bind.v2.model.util.ArrayInfoUtil;

import java.lang.reflect.Array;
import java.util.Scanner;

/**
 * Homework task 3.
 *
 * @author Damir
 */

public class TrzeciMain {

    /**
     * Application main method.
     *
     * @param args Application starting parameters
     */


    public static void main(String[] args) {
        int number; //Command from user.
        Scanner scanner = new Scanner(System.in); //For getting user input.
        System.out.println("Enter a number you want to convert, between 0 and 15: ");
        number = scanner.nextInt();
        //Application main loop.
        if (number < 0 || number > 15) {
            //Informing user if condition is false.
            System.out.println("Error, You should enter a number between 0 and 15: ");
        }
        //Adding and printing note if condition is true.
        else {
            // array to store binary number
            int tab[] = new int[4];
            // counter for binary array
            int a = 0;
            while (number > 0) {
                // storing remainder in binary array
                tab[a] = number % 2;
                number = number / 2;
                a++;
            }

            System.out.println("Converting to binary is:");
            // printing binary array in reverse order
            for (int i = tab.length-1; i >= 0; i--) {
                System.out.print(tab[i]);
            }

        }
    }
}
