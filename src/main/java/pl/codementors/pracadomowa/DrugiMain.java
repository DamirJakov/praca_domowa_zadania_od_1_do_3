package pl.codementors.pracadomowa;

import java.util.Scanner;

/**
 * Homework task 2.
 *
 * @author Damir
 */

public class DrugiMain {

    /**
     * Application main method.
     *
     * @param args Application starting parameters
     */
    public static void main(String[] args) {
        int number, maxNumber, minNumber; //Command from user.
        Scanner scanner =new Scanner(System.in); //For getting user input.
        System.out.println("Enter an array length greater than zero: ");
        //Application main loop.
        do {
            //Asking user to put right value/if condition is false.
            number = scanner.nextInt();
            if (number <= 0) {
                System.out.println("Error, array must be grater than zero: ");
            }
        }
        //Adding and printing note if condition is true.
        while (number <= 0);
        int tab[] = new int[number];
        for (int i = 0; i < number; i++){
            System.out.println("Enter the value for the " +i+ " index in the table");
            tab[i]=scanner.nextInt();
        } //searching for the smallest/minimum element
        minNumber = tab[0];
        for (int i = 1; i < number; i++){
            if (minNumber > tab[i]){
                minNumber = tab[i];
            }
        }
        // giving the result
        System.out.println("The smallest element in the array is: " + minNumber);
        //searching for the biggest/maximum element
        maxNumber = tab[0];
        for (int i = 1; i < number; i++) {
            if (maxNumber < tab[i]) {
                maxNumber = tab[i];
            }
        }
        System.out.println("The biggest element in the array is: " + maxNumber);

    }
}
